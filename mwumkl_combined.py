
from pyspark import SparkConf, SparkContext

import numpy as np
from numpy import array
import copy
import math
import sys
import random
import datetime

def format_x(raw_x):
	splitted_x = [float(i) for i in raw_x.split(",")]
	return [int(splitted_x[0]), splitted_x[1:-1], int(splitted_x[-1])]


class kernel(object):
	kernel_id = ""
	kernel_arg = 0
	trace = 0
	sigma = 0

	def __init__(self, kernel_id_, k_arg):
		self.kernel_id = kernel_id_
		self.kernel_arg = k_arg

	def kernelMethod(self):
		return 0

	def set_trace(self, X_):
		self.trace = X_.map(lambda line : self.kernelMethod(np.array(line[1]), np.array(line[1]))).sum()

class linearKernel(kernel):

	def __init__(self, k_arg):
		kernel_id = "linear_" + str(k_arg)
		kernel.__init__(self, kernel_id, k_arg)

	def kernelMethod(self, x_1, x_2):
		return np.dot(x_1, x_2) + self.kernel_arg

class polyKernel(kernel):

	def __init__(self, k_arg):
		kernel_id = "poly_" + str(k_arg)
		kernel.__init__(self, kernel_id, k_arg)

	def kernelMethod(self, x_1, x_2):
		return (np.dot(x_1, x_2) + 1) ** self.kernel_arg

class gaussKernel(kernel):

	def __init__(self, k_arg):
		kernel_id = "rbf_" + str(k_arg)
		kernel.__init__(self, kernel_id, k_arg)

	def kernelMethod(self, x_1, x_2):
		return math.exp(-1 * sum((x_1 - x_2) ** 2) / self.kernel_arg)



def construct_kernels(raw_kernels, X):

	kernels = list()
	for k in raw_kernels:
		k_name = k[0]
		k_arg = k[1]
		if k_name == "linear":
			kernel = linearKernel(k_arg)
		elif k_name == "poly" or k_name == "polynomial":
			kernel = polyKernel(k_arg)
		elif k_name == "rbf" or k_name == "gaussian":
			kernel = gaussKernel(k_arg)
		else:
			sys.exit("Error : Invalid kernel : " + k_name)

		kernel.set_trace(X)
		kernels.append(kernel)

	return kernels

class MWUMKLModel:
	alphaXY = list()
	kernels = list()

	def __init__(self, alphaXY_, kernels_):
		self.alphaXY = alphaXY_
		self.kernels = kernels_

	def predict(self, x_test):

		alphaXY_ = [i for i in self.alphaXY]
		kernels_ = [i for i in self.kernels]
		
		def predict_x(x_, alphaXY_, kernels_):
			pred = 0
			for kernel in kernels_:
				for i in alphaXY_:
			 		pred += (kernel.kernelMethod(x_, np.array(i[2])) * i[3] * i[1] * kernel.sigma)
			if pred > 0:
				return 1
			else:
				return -1

		y_pred = x_test.map(lambda line : [line[0], predict_x(np.array(line[1]), alphaXY_, kernels_), line[2]])
		tp = y_pred.filter(lambda line : line[1] == 1 and line[2] == 1).count()
		fp = y_pred.filter(lambda line : line[1] == 1 and line[2] == -1).count()
		fn = y_pred.filter(lambda line : line[1] == -1 and line[2] == 1).count()
		test_size = y_pred.count()
		print("True Positives : " + str(tp) + " / " + str(test_size))
		print("False Positives : " + str(fp) + " / " + str(test_size))
		print("False Negatives : " + str(fn) + " / " + str(test_size))
		precision_ = tp*1.0/(tp+fp)
		recall_ = tp*1.0/(tp+fn)
		f1 = 2.0*precision_*recall_/(precision_ + recall_)
		print("precision : " + str(precision_))
		print("recall : " + str(recall_))
		print("f1 : " + str(f1))
		

class MWUMKL(object):

	alphaGalpha = list()
	mu = list()
	sigma = list()
	primal_l11 = list()
	primal_l12 = list()
	primal_ea = 0

	def __init__(self):
		pass

	def mu_entropy(self):
		entropy = 0
		sum_ = 0
		for mu_ in self.mu:
			if mu_ > 0:
				sum_ += mu_
				entropy += -mu_ * math.log(mu_)
		entropy = entropy/sum_;
		entropy += math.log(sum_);
		entropy = entropy/math.log(len(self.mu));
		return entropy;

	def oracle(self, g):
		pos_max = g.filter(lambda line: line[2] > 0).map(lambda line: line[1]).max()
		neg_max = g.filter(lambda line: line[2] < 0).map(lambda line: line[1]).max()
		if pos_max + neg_max < -2:
			print("Oracle failed")
			sys.exit()
		pos_i = g.filter(lambda line: line[2] > 0 and pos_max == line[1]).first()[0]
		neg_i = g.filter(lambda line: line[2] < 0 and neg_max == line[1]).first()[0]
		return [pos_i, neg_i]


	def normalize_primal(self, bc_m, bc_n):
		primal_trace = sum(self.primal_l11) * 2
		primal_trace += bc_m.value * (bc_n.value - 1) * self.primal_ea;
		self.primal_l11 = [i*1.0/primal_trace for i in self.primal_l11]
		self.primal_l12 = [i*1.0/primal_trace for i in self.primal_l12]
		self.primal_ea = self.primal_ea*1.0/primal_trace;

	def exponentiateM(self, bc_m, bc_n, epsp, rho, cutoff):
		u_coeff = abs(-epsp/(2*rho))

		norm_u_raw = list()
		for i in range(bc_m.value):
			norm_u_raw.append(self.alphaGalpha[i]**0.5)

		norm_u = list()
		for ui in norm_u_raw:
			norm_u.append(u_coeff*ui)

		q = max(norm_u)
		
		self.primal_l11 = list()
		self.primal_l12 = list()

		if q > cutoff:
			for u in norm_u:
				temp = math.exp(u-q)/2
				self.primal_l11.append(temp)
				self.primal_l12.append(-temp)
			self.primal_ea = math.exp(-q)
		else:
			for u in norm_u:
				self.primal_l11.append(np.cosh(u))
				self.primal_l12.append(-np.sinh(u))
			self.primal_ea = 1;
		self.normalize_primal(bc_m, bc_n)

	def solve_mwumkl(self, sc, bc_n, bc_m, X, Y, alpha, g, G_Alpha, bc_kernels, rho, tau, epsp, c, Cmarg, norm1or2, cutoff):

		self.alphaGalpha = np.zeros(bc_m.value)
		
		print("Number of iterations : " + str(tau))
		for i in range(tau):
			max_g_index = self.oracle(g)
			bc_j1 = sc.broadcast(max_g_index[0])
			bc_j2 = sc.broadcast(max_g_index[1])
			alpha = alpha.map(lambda line : [line[0], line[1] + 0.5 if line[0] == bc_j1.value or line[0] == bc_j2.value else line[1]])

			xyj1 = X.filter(lambda line : line[0] == bc_j1.value).first()
			xyj2 = X.filter(lambda line : line[0] == bc_j2.value).first()
			bc_xj1 = sc.broadcast(np.array(xyj1[1]))
			bc_xj2 = sc.broadcast(np.array(xyj2[1]))
			bc_yj1 = sc.broadcast(xyj1[2])
			bc_yj2 = sc.broadcast(xyj2[2])

			for j in range(bc_m.value):
				kernel_method_j = bc_kernels.value[j].kernelMethod
				Kij1 = X.map(lambda line : kernel_method_j(bc_xj1.value, np.array(line[1])))
				Kij2 = X.map(lambda line : kernel_method_j(bc_xj2.value, np.array(line[1])))

				self.alphaGalpha[j] += G_Alpha.filter(lambda line: line[0] == bc_j1.value).first()[1][j] * 0.5
				self.alphaGalpha[j] += G_Alpha.filter(lambda line: line[0] == bc_j2.value).first()[1][j] * 0.5

				Kij = Kij1.zip(Kij2)
				ijk = Kij.map(lambda line : (line[0]*bc_yj1.value + line[1]*bc_yj2.value)*0.5)
				Kij1.unpersist()
				Kij2.unpersist()
				Kij.unpersist()
				
				yGalpha = Y.zip(G_Alpha)

				yGalpha = yGalpha.map(lambda line : [line[0][0], line[0][1], line[1][1]])
				yGalpha_ijk = yGalpha.zip(ijk)
				yGalpha.unpersist()

				kernel_j_trace = bc_kernels.value[j].trace

				def updateGAlpha(gAlpha_, ijk, yk, ji, j1, j2, c_, trace_, Cmarg_, norm1or2_):
					result = [g_ for g_ in gAlpha_[:j]]
					new_val = gAlpha_[j] + (ijk * c_ * yk / trace_)
					if norm1or2_ == 2 and (ji == j1 or ji == j2):
						result.append(new_val + (0.5 / Cmarg_))
					else:
						result.append(new_val)
					for gi in gAlpha_[j+1:]:
						result.append(gi)
					return [ji, result]

				G_Alpha = yGalpha_ijk.map(lambda line: updateGAlpha(line[0][2], line[1], line[0][1], line[0][0], bc_j1.value, bc_j2.value, c, kernel_j_trace, Cmarg, norm1or2))
				yGalpha_ijk.unpersist()

				self.alphaGalpha[j] += G_Alpha.filter(lambda line: line[0] == bc_j1.value).first()[1][j] * 0.5
				self.alphaGalpha[j] += G_Alpha.filter(lambda line: line[0] == bc_j2.value).first()[1][j] * 0.5
				G_Alpha.cache()

			self.exponentiateM(bc_m, bc_n, epsp, rho, cutoff)

			g = g.map(lambda line : [line[0], 0, line[2]])
			for j in range(bc_m.value):
				p = self.primal_l12[j]
				q = self.alphaGalpha[j]**2;

				gGAlpha = g.zip(G_Alpha)
				g = gGAlpha.map(lambda line : [line[0][0], line[0][1] + 2.0 * p * line[1][1][j] / q, line[0][2]])
				gGAlpha.unpersist()

			alpha.cache()
			g.cache()
			G_Alpha.cache()
			bc_j1.unpersist(blocking=True)
			bc_j2.unpersist(blocking=True)
			bc_xj1.unpersist(blocking=True)
			bc_xj2.unpersist(blocking=True)
			bc_yj1.unpersist(blocking=True)
			bc_yj2.unpersist(blocking=True)		

		print(str(tau) + " iterations completed")
		alpha = alpha.map(lambda line: [line[0], line[1]/tau])
		G_Alpha = G_Alpha.map(lambda line: [line[0], [i/tau for i in line[1]]])
		self.alphaGalpha = [i/(tau**2) for i in self.alphaGalpha]
		return [alpha, g, G_Alpha]

	def train(self, sc, X, Y, raw_kernels, eps = 0.2, c = 1.0, Cmarg = 0.5, norm1or2 = 2, cutoff = 20, ratio = 0.25):

		bc_kernels = sc.broadcast(construct_kernels(raw_kernels, X))
		bc_n = sc.broadcast(Y.count())
		bc_m = sc.broadcast(len(raw_kernels))

		alpha = Y.map(lambda line : [line[0], 0])
		g = Y.map(lambda line: [line[0], 0, line[1]])
		G_Alpha = Y.map(lambda line: [line[0], np.zeros(bc_m.value)])

		eps0 = .5
		epsp = - math.log(1 - eps0)

		rho = (c ** 0.5) / 2
		tau = int(math.ceil(2*ratio*rho*math.log(bc_n.value)/eps));
		
		mkl_result = self.solve_mwumkl(sc, bc_n, bc_m, X, Y, alpha, g, G_Alpha, bc_kernels, rho, tau, epsp, c, Cmarg, norm1or2, cutoff)

		alpha = mkl_result[0]
		g = mkl_result[1]
		G_Alpha = mkl_result[2]

		self.mu = np.zeros(bc_m.value)
		for i in range(bc_m.value):
			p = -self.primal_l12[i]
			q = self.alphaGalpha[i] ** 0.5
			self.mu[i] = 2.0 * p / q
		mu_sum = sum(self.mu)
		self.mu = [i/mu_sum for i in self.mu]

		g = g.map(lambda line : [line[0], 0, line[2]])
		# for i in range(bc_m.value):
		# 	if self.mu[i] == 0:
		# 		continue;
		# 	gGAlpha = g.zip(G_Alpha)
		# 	g = gGAlpha.map(lambda line : [line[0][0], line[0][1] + self.mu[i] + line[1][1][i], line[0][2]])
		gGAlpha = g.zip(G_Alpha)
		g = gGAlpha.map(lambda line : [line[0][0], sum([self.mu[i]*line[1][1][i] for i in range(bc_m.value)]), line[0][2]])




		gAlpha_combined = g.zip(alpha)
		pavg = gAlpha_combined.filter(lambda line : line[0][2] > 0).map(lambda line : line[0][1] * line[1][1]).sum()
		navg = gAlpha_combined.filter(lambda line : line[0][2] < 0).map(lambda line : line[0][1] * line[1][1]).sum()
		scale = pavg + navg
		bsvm = (navg - pavg)/(pavg + navg)
		alpha = alpha.map(lambda line : [line[0], line[1]/scale])

		entropy = self.mu_entropy()
		pos_mu_count = 0
		for mu_ in self.mu:
			if mu_ > 0:
				pos_mu_count += 1

		alphaX = alpha.zip(X).filter(lambda line: line[0][1] != 0).map(lambda line : [line[0][0], line[0][1], line[1][1], line[1][2]]).collect()

		final_kernels = list()
		for i in range(bc_m.value):
			if bc_kernels.value[i].trace == 0:
				continue
			sig = c * self.mu[i] / bc_kernels.value[i].trace
			if sig != 0:
				final_kernel = copy.deepcopy(bc_kernels.value[i])
				final_kernel.sigma = sig
				final_kernels.append(final_kernel)

		model = MWUMKLModel(alphaX, final_kernels) 

		print("mu : " + str(self.mu))
		print("Final Kernels and sigma : " + str([(k.kernel_id, k.sigma) for k in final_kernels]))
		print("bias : " + str(bsvm))
		print("omega : " + str(1.0/scale))
		print("mu entropy : " + str(entropy))

		return model

if __name__ == "__main__":

	conf = SparkConf()
#	conf.setMaster("local[*]")
	conf.setAppName("mwumkl")
	sc = SparkContext(conf=conf)

	train_data = sc.textFile("hdfs://mpserv3.cs.utah.edu:8020/user/sarath/bc/train.dat")
	test_data = sc.textFile("hdfs://mpserv3.cs.utah.edu:8020/user/sarath/bc/test.dat")

	x = train_data.map(lambda line : format_x(line))
	y = x.map(lambda line : [line[0], line[2]])
	#kernels = [["poly", 2], ["rbf", 2**2], ["rbf", 2**5], ["rbf", 2**8], ["rbf", 2**10]]
	#kernels = [["poly", 2], ["rbf", 2], ["rbf", 4]]
	n = x.count()

	kernels = [["poly", 2], ["rbf", 2**2], ["rbf", 2**5], ["rbf", 2**10], ["rbf", 15], ["rbf", n]]
	
	m = len(kernels)

	print("Number of partitions : " + str(x.getNumPartitions()))

	t_start = datetime.datetime.now()
	cls = MWUMKL() 
	mwu_model = cls.train(sc, x, y, kernels)
	t_end = datetime.datetime.now()
	t = t_end - t_start
	print("Took " + str(t.seconds) + " seconds to train")

	x_test = test_data.map(lambda line : format_x(line))
	y_test = x_test.map(lambda line : [line[0], line[2]])
	y_pred = mwu_model.predict(x_test)



