import math
import numpy as np
import sys

class kernel(object):
	kernel_id = ""
	kernel_arg = 0
	trace = 0
	sigma = 0

	def __init__(self, kernel_id_, k_arg):
		self.kernel_id = kernel_id_
		self.kernel_arg = k_arg

	def kernelMethod(self):
		return 0

	def set_trace(self, X_):
		self.trace = X_.map(lambda line : self.kernelMethod(np.array(line[1]), np.array(line[1]))).sum()

class linearKernel(kernel):

	def __init__(self, k_arg):
		kernel_id = "linear_" + str(k_arg)
		kernel.__init__(self, kernel_id, k_arg)

	def kernelMethod(self, x_1, x_2):
		return np.dot(x_1, x_2) + self.kernel_arg

class polyKernel(kernel):

	def __init__(self, k_arg):
		kernel_id = "poly_" + str(k_arg)
		kernel.__init__(self, kernel_id, k_arg)

	def kernelMethod(self, x_1, x_2):
		return (np.dot(x_1, x_2) + 1) ** self.kernel_arg

class gaussKernel(kernel):

	def __init__(self, k_arg):
		kernel_id = "rbf_" + str(k_arg)
		kernel.__init__(self, kernel_id, k_arg)

	def kernelMethod(self, x_1, x_2):
		return math.exp(-1 * sum((x_1 - x_2) ** 2) / self.kernel_arg)



def construct_kernels(raw_kernels, X):

	kernels = list()
	for k in raw_kernels:
		k_name = k[0]
		k_arg = k[1]
		if k_name == "linear":
			kernel = linearKernel(k_arg)
		elif k_name == "poly" or k_name == "polynomial":
			kernel = polyKernel(k_arg)
		elif k_name == "rbf" or k_name == "gaussian":
			kernel = gaussKernel(k_arg)
		else:
			sys.exit("Error : Invalid kernel : " + k_name)

		kernel.set_trace(X)
		kernels.append(kernel)

	return kernels

