from pyspark import SparkConf, SparkContext
import mwumkl as mkl
import datetime

if __name__ == "__main__":

	conf = SparkConf().setMaster("local[*]").setAppName("mwumkl")
	sc = SparkContext(conf=conf)

	train_data = sc.textFile("/Users/sarathkrishna/Desktop/mkl/data/pima/train3.data")
	test_data = sc.textFile("/Users/sarathkrishna/Desktop/mkl/data/pima/test3.data")

	x = train_data.map(lambda line : mkl.format_x(line))
	y = x.map(lambda line : [line[0], line[2]])
	#kernels = [["poly", 2], ["rbf", 2**2], ["rbf", 2**5], ["rbf", 2**8], ["rbf", 2**10]]
	#kernels = [["poly", 2], ["rbf", 2], ["rbf", 4]]
	n = x.count()

	kernels = [["poly", 2], ["rbf", 2], ["rbf", 2**2], ["rbf", 2**5], ["rbf", n]]
	
	m = len(kernels)

	print("Number of partitions : " + str(x.getNumPartitions()))

	t_start = datetime.datetime.now()
	cls = mkl.MWUMKL() 
	mwu_model = cls.train(sc, x, y, kernels)
	t_end = datetime.datetime.now()
	t = t_end - t_start
	print("Took " + str(t.seconds) + " seconds to train")

	x_test = test_data.map(lambda line : mkl.format_x(line))
	y_test = x_test.map(lambda line : [line[0], line[2]])
	y_pred = mwu_model.predict(x_test)
	# y_pred.saveAsTextFile("/Users/sarathkrishna/Desktop/mkl/bc/predictions")
